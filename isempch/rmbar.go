/*
isempchのテスト用
名前の頭に_がついていたら問答無用で削除します
*/
package main

import (
	"os"
	"regexp"
)

func main() {
	wd, _ := os.Getwd()
	d, _ := os.Open(wd)
	defer d.Close()
	r := regexp.MustCompile(`^_.*`)

	items, _ := d.Readdirnames(0)
	for _, v := range items {
		if r.MatchString(v) {
			oldnameB := []byte(v)
			newnameB := oldnameB[1:]
			newnameS := string(newnameB)
			os.Rename(v, newnameS)
		}
	}
}
