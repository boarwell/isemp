/*
2017-08-11: すでに_がついているディレクトリは除外したい
エラー処理をもっとすっきりさせたい
2017-08-11: 空でなくなったディレクトリの_を消せるようにしたい

実行時間計測は
http://kwmt27.net/index.php/2012/07/06/golang%E3%81%A7%E3%81%8B%E3%81%8B%E3%81%A3%E3%81%9F%E5%87%A6%E7%90%86%E6%99%82%E9%96%93%E3%82%92%E8%A8%88%E7%AE%97%E3%81%99%E3%82%8B%E3%81%AB%E3%81%AF%EF%BC%9F/
*/
package main

import (
	"fmt"
	"io"
	"os"
	"regexp"
	"time"
)

func main() {
	start := time.Now()

	// カレントディレクトリにあるディレクトリのリストを返す
	dirch := make(chan string, 3) // 3は適当
	go getdirs(dirch)

	// カレントディレクトリのディレクトリから中身がカラのものだけを抽出
	empList := make(chan string, 3) // 3は適当
	go makeEmpList(dirch, empList)

	// 中身がカラのディレクトリの頭に_をつける
	workquit := make(chan bool)
	go insertUnderbar(empList, workquit)

	// 中身がカラじゃないのに_がついているディレクトリの_を削除
	// makeEmpList()とdirchを共有していると、こっちが先に読んだチャネルの内容は
	// makeEmpList()が読めなくなって、不都合が出たため、こっちでは同じような処理を再び実装した
	rmbar()

	end := time.Now()
	fmt.Printf("%f秒\n", (end.Sub(start)).Seconds())
	// goroutineが終了するのを待つ
	<-workquit
}

// カレントディレクトリにあるディレクトリのリストを返す
func getdirs(dirch chan string) {
	cdir, _ := os.Getwd()
	f, _ := os.Open(cdir)
	defer f.Close()
	itemsInWd, _ := f.Readdirnames(0)

	taskquit := make(chan bool)

	go func() {
		for _, v := range itemsInWd {
			fi, _ := os.Stat(v)
			if fi.IsDir() {
				dirch <- v
			}
		}
		close(dirch)
		// ここのタスク終了用のチャネルは
		// sync.WaitGroupでもかけそう
		taskquit <- true
	}()
	<-taskquit
}

// カレントディレクトリのディレクトリから中身がカラのものだけを抽出
// 2017-08-16 戻り値が[]stirngだったのをchan stringに変更
func makeEmpList(dirch chan string, empList chan string) {

	for v := range dirch {
		func() {
			f, _ := os.Open(v)
			_, err := f.Readdirnames(1)
			if err == io.EOF {
				empList <- v
			}
			/*
				deferはLIFOキューに処理をのせるらため、
				メモリを大量に使うことになるためループの中では避けたほうがいいらしいです
				なのでここではdeferをやめておきます
				→2017-08-16
				クロージャを覚えましたのでdeferにしました
			*/
			defer f.Close()
		}()
	}
	// rangeでチャネルを使う場合はclose()が必須
	close(empList)
}

// 中身がカラのディレクトリの頭に_をつける
func insertUnderbar(empList chan string, workquit chan bool) {
	for v := range empList {
		// すでに_が頭についているディレクトリは除外する
		if string(v[0]) == "_" {
			continue
		}
		os.Rename(v, "_"+v)
	}
	workquit <- true
}

// 中身がカラじゃないのに_がついているディレクトリの_を削除
func rmbar() {
	wd, _ := os.Getwd()
	d, _ := os.Open(wd)
	defer d.Close()
	items, _ := d.Readdirnames(0)

	dirs := make(chan string, 3)
	go func() {
		for _, v := range items {
			fi, _ := os.Stat(v)
			if fi.IsDir() {
				dirs <- v
			}
		}
		close(dirs)
	}()

	for v := range dirs {
		go func(v string) {
			f, _ := os.Open(v)
			_, err := f.Readdirnames(1)
			// 中身がカラじゃない、かつ、名前が_からはじまる
			r := regexp.MustCompile(`^_.*`)
			if err != io.EOF && r.MatchString(v) {
				oldnameB := []byte(v)
				newnameB := oldnameB[1:]
				newnameS := string(newnameB)
				os.Rename(v, newnameS)
			}
			defer f.Close()
		}(v)
	}
}
