/*
2017-08-11: すでに_がついているディレクトリは除外したい
エラー処理をもっとすっきりさせたい
2017-08-11: 空でなくなったディレクトリの_を消せるようにしたい
*/
package main

import (
	"io"
	"os"
	"path/filepath"
	"regexp"
)

func main() {
	// カレントディレクトリにあるディレクトリのリストを返す
	dirs := getdirs()
	// カレントディレクトリのディレクトリから中身がカラのものだけを抽出
	empDirs := makeEmpList(dirs)
	// 中身がカラのディレクトリの頭に_をつける
	insertUnderbar(empDirs)
	// 中身がカラじゃないのに_がついているディレクトリの_を削除
	removeUnderbar(dirs)
}

// カレントディレクトリにあるディレクトリのリストを返す
func getdirs() []string {
	cdir, _ := filepath.Abs(".")
	f, _ := os.Open(cdir)
	// f.Readdirnames()はすべての要素がほしいときは0以下の数字
	itemsInWd, _ := f.Readdirnames(0)

	var dirs []string
	for _, v := range itemsInWd {
		fi, _ := os.Stat(v)
		if fi.IsDir() {
			dirs = append(dirs, fi.Name())
		}
	}
	return dirs
}

// カレントディレクトリのディレクトリから中身がカラのものだけを抽出
func makeEmpList(dirList []string) []string {
	var empList []string

	for _, v := range dirList {
		f, _ := os.Open(v)
		_, err := f.Readdirnames(1)
		if err == io.EOF {
			empList = append(empList, v)
		}
		/*
			deferはLIFOキューに処理をのせるらため、
			メモリを大量に使うことになるためループの中では避けたほうがいいらしいです
			なのでここではdeferをやめておきます
		*/
		f.Close()
	}
	return empList
}

// 中身がカラのディレクトリの頭に_をつける
func insertUnderbar(empList []string) {
	for _, v := range empList {
		// すでに_が頭についているディレクトリは除外する
		if string(v[0]) == "_" {
			continue
		}
		os.Rename(v, "_"+v)
	}
}

// 中身がカラじゃないのに_がついているディレクトリの_を削除
func removeUnderbar(dirList []string) {
	for _, v := range dirList {
		f, _ := os.Open(v)
		_, err := f.Readdirnames(1)
		// 中身がカラじゃない、かつ、名前が_からはじまる
		r := regexp.MustCompile(`^_.*`)
		if err != io.EOF && r.MatchString(v) {
			oldnameB := []byte(v)
			newnameB := oldnameB[1:]
			newnameS := string(newnameB)
			os.Rename(v, newnameS)
		}
		f.Close()
	}
}
